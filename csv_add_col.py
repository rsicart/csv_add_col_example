#!/usr/bin/env python

import csv

new_col_values = [
    'espagne',
    'france',
    'france',
]

with open('example.new.csv', 'w', newline='') as newcsvfile:
    writer = csv.writer(newcsvfile, delimiter=',')
    with open('example.csv', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        line_number = 0
        for line in reader:
            line.append(new_col_values[line_number])
            writer.writerow(line)
            line_number = line_number + 1
